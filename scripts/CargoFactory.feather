
abstract class CargoFactory : AbstractRole {

    @Attribute
    var doorName = "door"

    @Attribute
    var adjustBy = Vector2(0,1.0)

    override fun activated() {
        create()
    }

    override fun tick() {
    }

    
    fun create() {

        for (actor in Play.instance.rocksStage.actors) {
            if (actor.role is Door) {
                val door = actor.role as Door
                if (door.doorName == doorName ) {
                    door.close()
                }
            }
        }
        
    }

}

class WaterFactory : CargoFactory {

    override fun create() {
        super.create()

        val child = actor.createChild( "cargo" )
        val role = child.role as NewWaterCargo
        role.factory = this
        //actor.event("default")

    }
}

class GoldFactory : CargoFactory {

    @Attribute
    var hardness = 0

    override fun begin() {
        if ( hardness > Options.instance.hardness ) {
            actor.die()
        } else {
            Play.instance.goldRequired ++
            super.begin()
        }
    }

    override fun create() {
        actor.hide()
        val child = actor.createChild( "cargo" )
        val role = child.role as NewGoldCargo

        role.factory = this
        role.adjustBy.set( adjustBy )
    }
}


/**
    Created by a CargoFactory.
    An animation of the Cargo popping into existence, and then when it is done,
    the role is replaced by a Cargo.
*/
class NewGoldCargo : ActionRole() {

    var factory : CargoFactory = null
    val adjustBy = Vector2(0,1)

    override fun createAction() : Action {
        actor.scaleXY = 0.1
        return this:>adjust.action() whilst ScaleTo( actor, 1, 1, Eases.easeIn ) thenOnce
            this:>onCreated
    }

    fun adjust() : bool {
        if (Play.instance.overlappingSomethingDeadly( actor ) != null ) {
            actor.position += adjustBy
            return false
        }
        return false
    }
    fun onCreated() {
        actor.position += adjustBy
        val gold = Gold()
        actor.role = gold
        
        gold.factory = factory
        actor.event("created")
    }
}


/**
    Created by a CargoFactory.
    An animation of the Cargo popping out of the factory, and then when it is done,
    the role is replaced by a WaterCargo.
*/
class NewWaterCargo : ActionRole() {

    var factory : CargoFactory = null

    override fun createAction() : Action {
        return Delay(3) then
            this:>adjust thenOnce
            this:>onCreated
    }

    fun adjust() : bool {
        if (Play.instance.overlappingSomethingDeadly( actor ) != null ) {
            actor.position.y ++
            return false
        }
        return true
    }

    fun onCreated() {
        val wc = WaterCargo()
        actor.role = wc

        wc.factory = factory
        actor.event("created")
    }

}

