/**
    The gate where Cargo is deposited.

    Note, an ExitGate is where the ship enters to finish the scene.
    ExitGate and Gate are not related (other than they look similar, and have similar names).
*/
class Gate : AbstractRole {

    var isReady = true

    override fun tick() {
    }

    fun transportCargo( cargo : Cargo ) : bool {
        val faultyGate = Options.instance.faultyGate
        val speed = cargo.velocity.length()
        if ( faultyGate < 0 && speed > -faultyGate ) {
            // Too fast
            return false
        }
        if ( faultyGate > 0 && speed < faultyGate ) {
            // Too slow
            return false
        }

        actor.event( "sending" )
        cargo.actor.role = GateEntryAnimation( cargo.actor, actor, cargo:>sent )

        actor.createChild( "dangerZone" )

        return true
    }

    fun close() {
        isReady = false
        val barsA = actor.createChild("bars")
        (barsA.role as Bars).close()
    }
    
}

class GateEntryAnimation( val cargoActor : Actor, val gateActor : Actor, val postAction : () ) : ActionRole() {

    override fun createAction() : Action {
        val distance = gateActor.position - cargoActor.position

        return MoveBy( cargoActor.position, 0.2, distance, Eases.easeOut ) then
            ScaleTo( cargoActor, 0.3, 0.01 ) thenOnce
            postAction
    }

}
