Bugs
====

Rotate sound "clicks". Fade it out instead of stopping abruptly.

Thrust sound contiues to play post scene.
    Just stop-all for now!

Next
====

Create 9 Levels in total

When you start a new installment, chapterCompleted should reset back to 0
    (so that you can't jump to the end of the installment).
    The chaptersMenu must show ALL chapters if current is not the latest

Change the way installments/chapters work.
    To get to the next installment, you only need to complete chapters : Normal, High G and Total Inversion
    When you complete all 9 scenes using those 3 chapters, you unlock the other chapters.

Maybe Later
===========

Challenge levels
    Speed Runs
        A special Director, with options as attributes.
        Shows a high-score timer, and the current time.
            Can include existing scenes ;-)

        Set options (but don't save them)
            Slam Dunk - Faulty Gate option is very high
            Three Pointers - Danger Zone is large
            Back Court Lob - Danger Zone is VERY large
        Quitting the scene loads the options again.

    Requires a special Director :
        Longer and Longer
            See how many water cargos can be transported, the rod get longer each time.
            Gold icons show the high score
            Add a water icon each time water is transported.

Special Effects, which don't affect the game :
    Comets, shooting stars, stars, moons and suns in the sky.

