A game in the style of the classic BBC Micro game Thrust.
You control a space ship, and your job is to collect various resources,
and transport them through a gateway.
When you have completed the mission, enter the gateway to get to the next world.

![screenshot](screenshot.png)

As with the original thrust, the fun part is controlling the ball, rather shooting things.
So Higgs Anomaly has no aliens to kill.
When you've completed all the levels, prepare to do them again, but with different gravity.
But the variations in Anomaly are much weirder! (That's enough spoilers).

Implementation Notes
====================

This uses my game engine :
[Tickle](https://nickthecoder/software/tickle)

The game is written entirely in my own scripting language :
[Feather](https://nickthecoder/software/feather)

Together, these make writing games, quick and enjoyable.

Status
======

Just over 1,000 significant lines of code
(games written with Tickle tend to be quite small).

The code is mostly _finished_, and there are a reasonable amount of levels.

Tickle doesn't support drawing arbitrary polygons, so the caverns are made up of
lots of individual overlapping rock sections, which makes level design a PITA.
